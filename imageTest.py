
# Image analisys test.
# Author: David Atauri. Decembre 2017
# It gets an image and retrieve info trough third party api's
# run: >python imageTest.py

from bottle import route, run, request
import requests
import json

import imageTestSettings as settings


@route('/tags', method='POST')
def getTagsByPost():
    # reads the url and redirects
    url = request.forms.get('url')
    log("tags", url)
    normTags = getTags(url)
    # normTags = settings.example
    log("tags", str(normTags))

    return json.dumps(normTags)


def getTags(url):
    log("tags", "Search tags")
    # Call immaga to get image tags

    def normalizeTagsConfidence(tags):
        # Confidence in range 0..1 to be used in css

        # get max confidence
        confidences = [x['confidence'] for x in tags]
        high = max(confidences)

        # normalize
        for val in tags:
            val['confidence'] = val['confidence']/high
            log("verbose", str(val))

        ret = {"max": high/100, "list": tags}
        return ret  # normalized
    # -----
    call = settings.IMAGGA_ENDPOINT \
        + settings.IMAGGA_TAGS \
        + url
    log("info", "Imagga call: "+call)
    tags = []

    response = requests.get(call, auth=(settings.IMAGA_API_KEY,
                                        settings.IMAGGA_SECRET))
    js = response.json()
    tags = normalizeTagsConfidence(js['results'][0]['tags'])

    return tags


@route('/hi')
def sayHi():
    # Resource to test if API is reacheble
    return "hi"


def launchApi():
    run(host=settings.HOST, port=settings.PORT)


def log(tag, msg):
    if tag in settings.LOG_TAGS:
        print("\n> "+msg)


# Main: Launches the api - - - -
launchApi()
# u = "http://images.unsplash.com/photo-1449034446853-66c86144b0ad?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=23b3198ebbdaddd015b85db8a81d8072"
# chiquito
u = "http://static3.ideal.es/www/multimedia/201710/16/media/cortadas/chiquito-kfKD--624x549@Ideal.jpg"
# getTags(u)
