# Image Tags example

This is a web app that retrieves tags for an image url. Tags are created by thrird party API: Imagga

# Requirements
Tested on linux:
 
* web server: like apache
* php
* python 2.7

# Set up

1. Clone in your web server htdocs (Ex: /var/www/html/tags)
2.  Create a python virtualenv, activate it and pip requirements: pip -r patht_to/requirements.txt
3. Launch the server 

> python imageTest.py

4. load the html in a web browser: http://localhost/tags/imageTest.html

# Enjoy !

* click on a link or paste an image url in text input field
* choose absolute or relative values of confidence
* Click on button to refresh
* rollover the tags to see the confidence value.